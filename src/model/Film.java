package model;

public class Film {
	private int filmId;
	private String title;
	private String description;
	private String language;
	private int releaseYear;
	private int languageId;
	private int rentalDuration;
	private int rentalRate;
	private int replacementCost;
	private int length;
	private String rating = "G";
	
	public int getFilmId() {
		return filmId;
	}
	public void setFilmId(int filmId) {
		this.filmId = filmId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getReleaseYear() {
		return releaseYear;
	}
	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}
	public int getLanguageId() {
		return languageId;
	}
	public String getLanguageName() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}
	public int getRentalDuration() {
		return rentalDuration;
	}
	public void setRentalDuration(int rentalDuration) {
		this.rentalDuration = rentalDuration;
	}
	public float getRentalRate() {
		System.out.println("GET: rentalRate -> " + rentalRate + " " + rentalRate / 100f);
		return rentalRate / 100f;
	}
	public void setRentalRate(float rentalRate) {
		System.out.println("SET: rentalRate -> " + rentalRate + " " + (int) (rentalRate * 100));
		this.rentalRate = (int)(rentalRate * 100);
	}
	public float getReplacementCost() {
		//System.out.println("GET: replacementCost -> " + replacementCost + " " + (replacementCost / 100f));
		return (replacementCost / 100f);
	}
	public void setReplacementCost(float replacementCost) {
		//System.out.println("SET: replacementCost -> " + replacementCost + "  " + ((int) (replacementCost * 100)));
		this.replacementCost = ((int) (replacementCost * 100));
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	
}
